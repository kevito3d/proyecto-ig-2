package proyecto_floristeria.modelo;

public class detalleventas {
    int iddetalleventa; 
    int idventas;
     int idproductos;
     int cantidad;
     Double precio;

    public detalleventas() {
    }

    public detalleventas(int iddetalleventa, int idventas, int idproductos, int cantidad, Double precio) {
        this.iddetalleventa = iddetalleventa;
        this.idventas = idventas;
        this.idproductos = idproductos;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public int getIddetalleventa() {
        return iddetalleventa;
    }

    public void setIddetalleventa(int iddetalleventa) {
        this.iddetalleventa = iddetalleventa;
    }

    public int getIdventas() {
        return idventas;
    }

    public void setIdventas(int idventas) {
        this.idventas = idventas;
    }

    public int getIdproductos() {
        return idproductos;
    }

    public void setIdproductos(int idproductos) {
        this.idproductos = idproductos;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
     
     
}
