/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_floristeria.modelo;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class clientedao {
    Connection con;
   coneccion cnon = new coneccion();
    PreparedStatement ps;
    ResultSet rs;

    public cliente listarid(String cedula){
        cliente c=new cliente();
    String sql="select * from cliente where cedula=?";
        try {
           con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setString(1, cedula);
            rs=ps.executeQuery(); 
            while (rs.next()) {                
                
                c.setIdcliente(rs.getInt(1));
                c.setCedula(rs.getString(2));
                c.setNombres(rs.getString(3));
                c.setDireccion(rs.getString(4));
                c.setTelefono(rs.getString(5));
                c.setEstado(rs.getString(6));
            }
        } catch (Exception e) {
        }
        return c;
    } 
            
    public List listar() {
    List<cliente> lista = new ArrayList<>();
    String sql="select * from cliente";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {                
                cliente c = new cliente();
                c.setIdcliente(rs.getInt(1));
                c.setCedula(rs.getString(2));
                c.setNombres(rs.getString(3));
                c.setDireccion(rs.getString(4));
                c.setTelefono(rs.getString(5));
                c.setEstado(rs.getString(6));
                lista.add(c);
            }
        } catch (Exception e) {
        }
        return lista;
    }

    public int add(Object[] o) {
        int r=0;
        String sql="insert into cliente(cedula,nombres,direccion,telefono,estado)values(?,?,?,?,?)";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setObject(1, o[0]);
            ps.setObject(2, o[1]);
            ps.setObject(3, o[2]);
            ps.setObject(4, o[3]);
            ps.setObject(5, o[4]);
            r=ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public int actualizar(Object[] o) {
     int r=0;
        String sql="update cliente set cedula=?,nombres=?,direccion=?,telefono=?,estado=? where idcliente=?";
        try {
             con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setObject(1, o[0]);
            ps.setObject(2, o[1]);
            ps.setObject(3, o[2]);
            ps.setObject(4, o[3]);
            ps.setObject(5, o[4]);
            ps.setObject(6, o[5]);
            r=ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public void eliminar(int idcliente) {
    String sql="delete from cliente where idcliente=?";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idcliente);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
           
}
