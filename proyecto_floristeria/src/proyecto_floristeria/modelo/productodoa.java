package proyecto_floristeria.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class productodoa {
    Connection con;
    coneccion cnon = new coneccion();
    PreparedStatement ps;
    ResultSet rs; 
    int r;
    
    public int actualizarstock(int cant,int idproducto){
        String sql="update productos set stock=? where idproducto=?";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
             ps.setInt(1, cant);
             ps.setInt(2,idproducto);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }
      public producto listarid(int idproducto){
        producto pr=new producto();
    String sql="select * from productos where idproducto=?";
        try {
           con=cnon.conectar();
            ps=con.prepareStatement(sql);
             ps.setInt(1, idproducto);
            rs=ps.executeQuery(); 
            while (rs.next()) {                
                
                pr.setIdproducto(rs.getInt(1));
                pr.setNombre(rs.getString(2));
                pr.setPrecio(rs.getDouble(3));
                pr.setStock(rs.getInt(4));
                pr.setEstado(rs.getString(5));
            }
        } catch (Exception e) {
        }
        return pr;
    } 
    
    public List listar() {
       List<producto> lista = new ArrayList<>();
    String sql="select * from productos";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {                
               producto p=new producto();
                p.setIdproducto(rs.getInt(1));
                p.setNombre(rs.getString(2));
                p.setPrecio(rs.getDouble(3));
                p.setStock(rs.getInt(4));
                p.setEstado(rs.getString(5));
                lista.add(p);
            }
        } catch (Exception e) {
        }
        return lista;
    }

    public int add(Object[] o) {
       r=0;
        String sql="insert into productos(nombres,precio,stock,estado)values(?,?,?,?)";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setObject(1, o[0]);
            ps.setObject(2, o[1]);
            ps.setObject(3, o[2]);
            ps.setObject(4, o[3]);      
            r=ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public int actualizar(Object[] o) {
         r=0;
        String sql="update productos set nombres=?,precio=?,stock=?,estado=? where idproducto=?";
        try {
             con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setObject(1, o[0]);
            ps.setObject(2, o[1]);
            ps.setObject(3, o[2]);
            ps.setObject(4, o[3]);
            ps.setObject(5, o[4]);
            r=ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public void eliminar(int idproducto) {
         String sql="delete from productos where idproducto=?";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idproducto);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    
}
