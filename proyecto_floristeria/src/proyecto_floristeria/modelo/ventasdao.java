
package proyecto_floristeria.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ventasdao {
    coneccion cn= new coneccion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    int r=0;
    
    public String nroserieventas(){
        
        String serie="";
        String sql="select max(numeroSerie) from ventas";
        try {
            con=cn.conectar();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) { 
            serie=rs.getString(1);
            } 
        } catch (Exception e) {
        }
return serie;
    }
    public String idventas(){
    String idv="";
    String sql="select max(idventas) from ventas";
        try {
                        con=cn.conectar();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) { 
            idv=rs.getString(1);
            }
        } catch (Exception e) {
        }
        return  idv;
    }
    
    public int guardarventas(ventas v){
    ventas ven= new ventas();
    String sql="insert into ventas(idcliente ,idvendedor ,numeroSerie ,fechaVentas ,subtotal ,iva ,total) values (?,?,?,?,?,?,?)";
        try {
            con=cn.conectar();
            ps=con.prepareStatement(sql);
            ps.setInt(1, v.getIdcliente());
            ps.setInt(2, v.getIdvendedor());
            ps.setString(3, v.getSerie());
            ps.setString(4, v.getFecha());
            ps.setDouble(5, v.getSubtotal());
            ps.setDouble(6, v.getIva());
            ps.setDouble(7, v.getTotal());
            r=ps.executeUpdate();
        } catch (Exception e) {
        }
    return r;
    }
       
    public int guardardetalleventas(detalleventas dv){
        String sql="insert into detalleventas(idventas  ,idproducto  ,cantidad ,precio) values (?,?,?,?)";
        try {
            con=cn.conectar();
            ps=con.prepareStatement(sql);
            ps.setInt(1, dv.getIdventas());
            ps.setInt(2, dv.getIdproductos());
            ps.setInt(3, dv.getCantidad());
            ps.setDouble(4, dv.getPrecio());
            r=ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }
    public List listar() {
       List<vista> lista = new ArrayList<>();
    String sql="select * from lista";
        try {
            con=cn.conectar();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {                
               vista v=new vista();
                v.setCedula(rs.getString(1));
                v.setNombres(rs.getString(2));
                v.setTelefono(rs.getString(3));
                v.setSerie(rs.getString(4));
                v.setFecha(rs.getString(5));
                v.setSubtotal(rs.getDouble(6));
                v.setIva(rs.getDouble(7));
                v.setTotal(rs.getDouble(8));
                lista.add(v);
                }
        } catch (Exception e) {
        }
        return lista; 
    }
 
}

    
