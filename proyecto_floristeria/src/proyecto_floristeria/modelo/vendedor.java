/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_floristeria.modelo;

/**
 *
 * @author pc
 */
public class vendedor {
    int         idvendedor;
    String      cedula ;
    String	nombres ;
    String	telefono;

    public vendedor() {
    }

    public vendedor(int idvendedor, String cedula, String nombres, String telefono) {
        this.idvendedor = idvendedor;
        this.cedula = cedula;
        this.nombres = nombres;
        this.telefono = telefono;
    }

    public int getIdvendedor() {
        return idvendedor;
    }

    public void setIdvendedor(int idvendedor) {
        this.idvendedor = idvendedor;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

   
}
