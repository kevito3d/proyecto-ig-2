
package proyecto_floristeria.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class vendedordao {
 Connection con;
   coneccion cnon = new coneccion();
    PreparedStatement ps;
    ResultSet rs;

    public List listar() {
    List<vendedor> lista = new ArrayList<>();
    String sql="select * from vendedor";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {                
                vendedor ve = new vendedor();
                ve.setIdvendedor(rs.getInt(1));
                ve.setCedula(rs.getString(2));
                ve.setNombres(rs.getString(3));
                ve.setTelefono(rs.getString(4));
                lista.add(ve);
            }
        } catch (Exception e) {
        }
        return lista;
    }

    public int add(Object[] o) {
        int r=0;
        String sql="insert into vendedor(cedula,nombres,telefono)values(?,?,?)";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setObject(1, o[0]);
            ps.setObject(2, o[1]);
            ps.setObject(3, o[2]);
            r=ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public int actualizar(Object[] o) {
     int r=0;
        String sql="update vendedor set cedula=?,nombres=?,telefono=? where idvendedor=?";
        try {
             con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setObject(1, o[0]);
            ps.setObject(2, o[1]);
            ps.setObject(3, o[2]);
            ps.setObject(4, o[3]);
            r=ps.executeUpdate();
        } catch (Exception e) {
        }
        return r;
    }

    public void eliminar(int idvendedor) {
    String sql="delete from vendedor where idvendedor=?";
        try {
            con=cnon.conectar();
            ps=con.prepareStatement(sql);
            ps.setInt(1, idvendedor);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
    
}
