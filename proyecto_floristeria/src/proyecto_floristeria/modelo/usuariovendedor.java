/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_floristeria.modelo;

/**
 *
 * @author pc
 */
public class usuariovendedor {
     int    idvendedor;
     String cedula;
     String contra;
     String user;

    public usuariovendedor() {
    }

    public usuariovendedor(int idvendedor, String cedula, String contra, String user) {
        this.idvendedor = idvendedor;
        this.cedula = cedula;
        this.contra = contra;
        this.user = user;
    }

    public int getIdvendedor() {
        return idvendedor;
    }

    public void setIdvendedor(int idvendedor) {
        this.idvendedor = idvendedor;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getContra() {
        return contra;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

   

   
     
}
