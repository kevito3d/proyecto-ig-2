
package proyecto_floristeria.vista;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import proyecto_floristeria.modelo.cliente;
import proyecto_floristeria.modelo.clientedao;
import javax.swing.table.DefaultTableModel;
import proyecto_floristeria.modelo.detalleventas;
import proyecto_floristeria.modelo.producto;
import proyecto_floristeria.modelo.productodoa;
import proyecto_floristeria.modelo.usuariovendedor;
import proyecto_floristeria.modelo.usuariovendedordao;
import proyecto_floristeria.modelo.vendedor;
import proyecto_floristeria.modelo.vendedordao;
import proyecto_floristeria.modelo.ventas;
import proyecto_floristeria.modelo.ventasdao;

public class vventas extends javax.swing.JInternalFrame {

    clientedao cldao=new clientedao();
    cliente clien=new cliente();
    productodoa pldao=new productodoa();
    DefaultTableModel modelo=new DefaultTableModel();
    ventas vn= new ventas();
    ventasdao vndao=new ventasdao();
    detalleventas dvn=new detalleventas();
     producto pl=new producto();

    double subt,pre,tota,iva;
    int cant,idproducto;
    public vventas() {
        initComponents(); 
       generarserie();
        fecha();
    }
void fecha(){
     Calendar cal=new GregorianCalendar();
        txtfecha.setText(""+cal.get(cal.YEAR)+"-"+cal.get(cal.MONTH)+"-"+cal.get(cal.DAY_OF_MONTH));
}
    
    void generarserie(){
        String serie=vndao.nroserieventas();
        if (serie==null) {
            txtserie.setText("0000001");
        } else {
            int incre=Integer.parseInt(serie);
            incre=incre+1;
            txtserie.setText("000000"+incre);
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField3 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtserie = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtcedula = new javax.swing.JTextField();
        bnbuscarcliente = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtcliente = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtcodproducto = new javax.swing.JTextField();
        bnbuscarproducto = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        txttelefono = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtnombreproducto = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtprecio = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtstock = new javax.swing.JTextField();
        bnagregar = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        txtcantidad = new javax.swing.JSpinner();
        jLabel13 = new javax.swing.JLabel();
        txtfecha = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtvendedor = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtsubtotal = new javax.swing.JTextField();
        txtiva = new javax.swing.JTextField();
        txttotal = new javax.swing.JTextField();
        bncancelar = new javax.swing.JButton();
        bnventas = new javax.swing.JButton();

        jTextField3.setText("jTextField3");

        setClosable(true);
        setTitle("SISTEMA DE VENTAS");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyecto_floristeria/img/IMG_20191121_151936.png"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Nº SERIE");

        txtserie.setEditable(false);
        txtserie.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("FACTURA 001-001");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 319, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtserie, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtserie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)))
        );

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("C.C Cliente:");

        bnbuscarcliente.setText("Buscar");
        bnbuscarcliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bnbuscarclienteActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Cliente");

        txtcliente.setEditable(false);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Direccion");

        txtdireccion.setEditable(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Cod. Producto:");

        bnbuscarproducto.setText("Buscar");
        bnbuscarproducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bnbuscarproductoActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("Telefono");

        txttelefono.setEditable(false);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Producto");

        txtnombreproducto.setEditable(false);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Precio:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Stock");

        txtstock.setEditable(false);

        bnagregar.setText("Agregar");
        bnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bnagregarActionPerformed(evt);
            }
        });

        jLabel12.setText("Cantidad");

        jLabel13.setText("Fecha");

        txtfecha.setEditable(false);

        jLabel14.setText("Vendedor");

        txtvendedor.setEditable(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtdireccion)
                    .addComponent(txtcliente, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtcedula, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bnbuscarcliente, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttelefono))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtfecha, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtprecio, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                                    .addComponent(txtcodproducto, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(bnagregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bnbuscarproducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel14))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtstock, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtvendedor)
                            .addComponent(txtnombreproducto, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtcedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bnbuscarcliente)
                    .addComponent(jLabel8)
                    .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtcliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtcodproducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bnbuscarproducto)
                    .addComponent(jLabel9)
                    .addComponent(txtnombreproducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtprecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(txtstock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bnagregar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(txtfecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txtvendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NRO", "ID Producto", "Cant.", "Descripcion", "P. Unitario", "P. Total"
            }
        ));
        tabla.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tabla.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tabla);
        if (tabla.getColumnModel().getColumnCount() > 0) {
            tabla.getColumnModel().getColumn(3).setMinWidth(227);
            tabla.getColumnModel().getColumn(4).setMinWidth(100);
            tabla.getColumnModel().getColumn(5).setMinWidth(100);
        }

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel15.setText("Total a Pagar");

        jLabel16.setText("Subtotal");

        jLabel17.setText("I.V.A 12%");

        bncancelar.setText("Cancelar");

        bnventas.setText("Generar Venta");
        bnventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bnventasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bnventas)
                .addGap(77, 77, 77)
                .addComponent(bncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17)
                    .addComponent(jLabel15))
                .addGap(22, 22, 22)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtsubtotal)
                    .addComponent(txtiva)
                    .addComponent(txttotal, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtsubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txtiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bncancelar)
                    .addComponent(bnventas))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bnbuscarclienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bnbuscarclienteActionPerformed
      buscarcliente();
    }//GEN-LAST:event_bnbuscarclienteActionPerformed

    private void bnbuscarproductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bnbuscarproductoActionPerformed
        buscarproducto();
    }//GEN-LAST:event_bnbuscarproductoActionPerformed

    private void bnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bnagregarActionPerformed
        agregarproducto();
    }//GEN-LAST:event_bnagregarActionPerformed

    private void bnventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bnventasActionPerformed
        if (txttotal.getText().equals("")) {
            JOptionPane.showMessageDialog(this,"Debe ingresar datos");
        } else {
         guardarventa();
        guardardetalleventas();
        actualizarstock();
        JOptionPane.showMessageDialog(this,"Exito en las Ventas");
        limpiarcampo();
        generarserie();
        }
    }//GEN-LAST:event_bnventasActionPerformed
void limpiarcampo(){
     limpiartabla();
    txtcedula.setText("");
    txttelefono.setText("");
    txtcliente.setText("");
    txtdireccion.setText("");
    txtnombreproducto.setText("");
    txtcodproducto.setText("");
    txtprecio.setText("");
    txtstock.setText("");
    txtcantidad.setValue(1);
    txtsubtotal.setText("");
    txtiva.setText("");
    txttotal.setText("");
    txtcedula.requestFocus();
     
}
    void limpiartabla(){
        for (int i = 0; i < modelo.getRowCount(); i++) {
            modelo.removeRow(i);
            i=i-1;
        }
    }
    void actualizarstock(){
    for (int i = 0; i < modelo.getRowCount(); i++) {
        producto pr=new producto();
       idproducto=Integer.parseInt(tabla.getValueAt(i, 1).toString());
       cant=Integer.parseInt(tabla.getValueAt(i, 2).toString());
       pr= pldao.listarid(idproducto);
       int sta=(pr.getStock()-cant);
       pldao.actualizarstock(sta, idproducto);
    }
}
    
    void guardarventa(){
    int cli=clien.getIdcliente();
    int ven=1;
    String ser=txtserie.getText();
    String fecha=txtfecha.getText();
    double sub=subt;
    double iv=iva;
    double tot=tota;
    
    vn.setIdcliente(cli);
    vn.setIdvendedor(ven);
    vn.setSerie(ser);
    vn.setFecha(fecha);
    vn.setSubtotal(sub);
    vn.setIva(iv);
    vn.setTotal(tot);
    vndao.guardarventas(vn);
}
void guardardetalleventas(){
    String idv=vndao.idventas();
    int idven=Integer.parseInt(idv);
    for (int i = 0; i < tabla.getRowCount(); i++) {
         idproducto = Integer.parseInt(tabla.getValueAt(i,1).toString());
        int cant = Integer.parseInt(tabla.getValueAt(i,2).toString());
        double pre = Double.parseDouble(tabla.getValueAt(i,4).toString());
        dvn.setIdventas(idven);
        dvn.setIdproductos(idproducto);
        dvn.setCantidad(cant);
        dvn.setPrecio(pre);
        vndao.guardardetalleventas(dvn);
    }
}
    void agregarproducto(){
    int nro=0;
    double total;
modelo=(DefaultTableModel)tabla.getModel();
 nro=nro+1;
 idproducto=pl.getIdproducto();
 String nomp=txtnombreproducto.getText();
 pre=Double.parseDouble(txtprecio.getText()); 
 cant=Integer.parseInt(txtcantidad.getValue().toString());
 int sto=Integer.parseInt(txtstock.getText());
 int ca=Integer.parseInt(txtcantidad.getValue().toString());
 total=cant*pre;
        if (txtcedula.getText().equals("")) {
            buscarcliente();
        }else{
                ArrayList lista=new ArrayList();
    if (sto>0) {
        if (ca>0) {
        lista.add(nro);
        lista.add(idproducto);
        lista.add(cant);
        lista.add(nomp);
        lista.add(pre);
        lista.add(total);
        Object[]ob=new Object[6];
        ob[0]=lista.get(0);
        ob[1]=lista.get(1);
        ob[2]=lista.get(2);
        ob[3]=lista.get(3);
        ob[4]=lista.get(4);
        ob[5]=lista.get(5);
        modelo.addRow(ob);
        tabla.setModel(modelo);
        calculartotal();
    }else{
    JOptionPane.showMessageDialog(this,"Debe Ingresar Cantidad");
    }    
    }else{
    JOptionPane.showMessageDialog(this,"No Disponible");
    }
    }
}
    void calculartotal(){
    subt=0.0000d;
    tota=0.0000d;
    iva=0.0000d;
        for (int i = 0; i < tabla.getRowCount(); i++) {
            cant=Integer.parseInt(tabla.getValueAt(i, 2).toString());
            pre=Double.parseDouble(tabla.getValueAt(i, 4).toString());
           subt=subt+(cant*pre); 
           iva=iva+((cant*pre)*0.12);
           tota=tota+(subt+iva);
        }
        txtsubtotal.setText(""+subt);
        txtiva.setText(""+iva);
        txttotal.setText(""+tota);
    }
    void  buscarproducto(){
 idproducto=Integer.parseInt(txtcodproducto.getText());
    if (txtcodproducto.getText().equals("")) {
        JOptionPane.showMessageDialog(this,"Debe Ingresar Datos de Producto");
    }else{
     pl=pldao.listarid(idproducto);
        if (pl.getIdproducto()!=0) {
           txtnombreproducto.setText(pl.getNombre());
           txtprecio.setText(""+pl.getPrecio());
           txtstock.setText(""+pl.getStock());
        } else {
            JOptionPane.showMessageDialog(this, "Producto no Registrado");
           txtcodproducto.requestFocus(); 
                }
        }
 }
    
    void buscarcliente(){
    int r;
String cedula=txtcedula.getText();
    if (txtcedula.getText().equals("")) {
        JOptionPane.showMessageDialog(this,"Debe Ingresar Datos de Cedula");
    }else{
     clien=cldao.listarid(cedula);
        if (clien.getCedula()!=null) {
           txtcliente.setText(clien.getNombres());
           txttelefono.setText(clien.getTelefono());
           txtdireccion.setText(clien.getDireccion());
          // txtvendedor.setText(ven.getNombres());
           txtcedula.requestFocus();
        } else {
            r=JOptionPane.showConfirmDialog(this, "Cliente no Registrado, Desea Registrar?");
            if (r==0) {
              vcliente vcl=new vcliente();
              principal.vprincipal.add(vcl);
              vcl.setVisible(true);
            } 
        }
    }
}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bnagregar;
    private javax.swing.JButton bnbuscarcliente;
    private javax.swing.JButton bnbuscarproducto;
    private javax.swing.JButton bncancelar;
    private javax.swing.JButton bnventas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTable tabla;
    private javax.swing.JSpinner txtcantidad;
    private javax.swing.JTextField txtcedula;
    private javax.swing.JTextField txtcliente;
    private javax.swing.JTextField txtcodproducto;
    private javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtfecha;
    private javax.swing.JTextField txtiva;
    private javax.swing.JTextField txtnombreproducto;
    private javax.swing.JTextField txtprecio;
    private javax.swing.JTextField txtserie;
    private javax.swing.JTextField txtstock;
    private javax.swing.JTextField txtsubtotal;
    private javax.swing.JTextField txttelefono;
    private javax.swing.JTextField txttotal;
    private javax.swing.JTextField txtvendedor;
    // End of variables declaration//GEN-END:variables
}
